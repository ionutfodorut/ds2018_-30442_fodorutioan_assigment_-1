<%@ page import="entities.Flight" %>
<%@ page import="java.util.List" %>
<%@ page import="antlr.FileLineFormatter" %>
<%@ page import="java.util.LinkedList" %>
<%@ page import="java.net.URL" %>
<%@ page import="java.net.HttpURLConnection" %><%--
  Created by IntelliJ IDEA.
  User: ioan
  Date: 29.10.2018
  Time: 01:31
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>:: Home ::</title>

    <script type="text/javascript">
        function showArrivalTime() {
            document.getElementById('arrivalTime').style.display = "block";        }
    </script>
    <script type="text/javascript">
        function showDepartureTime() {
            document.getElementById('departureTime').style.display = "block";
        }
    </script>

    <script type="text/javascript">
        var apikey = 'AIzaSyCWrTe-HpS0CpUVtwqhiZnDLPQ2Z9D4uqM'
        function getLocalTime(date, latitude, longitude,id){
            var datetime = new Date(date)
            var timestamp = datetime.getTime()/1000 + datetime.getTimezoneOffset() * 60; // Current UTC date/time expressed as seconds since midnight, January 1, 1970 UTC
            var apicall = 'https://maps.googleapis.com/maps/api/timezone/json?location=' + latitude + ',' + longitude + '&timestamp=' + timestamp + '&key=' + apikey;
            console.log(apicall);
            var xhr = new XMLHttpRequest(); // create new XMLHttpRequest2 object
            xhr.open('GET', apicall); // open GET request
            xhr.onload = function(){
                if (xhr.status === 200){ // if Ajax request successful
                    var output = JSON.parse(xhr.responseText); // convert returned JSON string to JSON object
                    console.log(output.status); // log API return status for debugging purposes
                    if (output.status == 'OK'){ // if API reports everything was returned successfully
                        var offsets = output.dstOffset * 1000 + output.rawOffset * 1000; // get DST and time zone offsets in milliseconds
                        var finalDate = new Date(timestamp * 1000 + offsets).toLocaleTimeString();
                        console.log(finalDate);
                        document.getElementById(id).innerHTML = finalDate;
                    }
                }
                else{
                    alert('Request failed.  Returned status of ' + xhr.status);
                }
            }
            xhr.send(); // send request
        }
    </script>
</head>
<body>
    <h2>
        Hi, <%= request.getAttribute("username") %>
    </h2>

<table width="100%">
    <tr bgcolor=F9F9F3>
        <td align = "center">
            <th><input type="submit" value="Get local departure time" onclick="showDepartureTime();"></th>
            <th><input type="submit" value="Get local arrival time" onclick="showArrivalTime();"></th>
        </td>
    </tr>
    <tr>
        <td>
        <table width = "100%" border = "1" align = "center">
            <tr bgcolor = "#949494">
                <th>Flight ID</th>
                <th>Airplane type</th>
                <th>Departure city</th>
                <th>Departure date and time</th>
                <th>Arrival city</th>
                <th>Arrival date and time</th>
            </tr>
            <c:forEach items="${flights}" var="flight">
                <tr>
                    <td><c:out value="${flight.id}" /></td>
                    <td><c:out value="${flight.airplaneType}" /></td>
                    <td><c:out value="${flight.departureCity.name}" /></td>
                    <td><c:out value="${flight.departureDate}" /></td>
                    <td><c:out value="${flight.arrivalCity.name}" /></td>
                    <td><c:out value="${flight.arrivalDate}" /></td>
                </tr>
            </c:forEach>
        </table>
        </td>

        <td>
            <table id="departureTime" style="display:none;" width = "100%" border = "1" align = "center">
                <tr bgcolor = "#949494" align = "center">
                    <th>Flight ID</th>
                    <th>Local departure date and time</th>
                </tr>
                <tr>
                    <c:forEach items="${flights}" var="flight">
                        <tr>
                            <td><c:out value="${flight.id}"/></td>
                            <td><p id="departureTime${flight.id}"></p></td>
                            <script type="text/javascript">
                                getLocalTime('${flight.departureDate}','${flight.departureCity.latitude}','${flight.departureCity.longitude}','departureTime${flight.id}');
                            </script>
                        </tr>
                    </c:forEach>
                </tr>
            </table>
        </td>
        <td>
            <table id="arrivalTime" style="display:none;" width = "100%" border = "1" align = "center">
                <tr bgcolor = "#949494" align = "center">
                    <th>Flight ID</th>
                    <th>Local arrival date and time</th>
                </tr>
                <tr>
                    <c:forEach items="${flights}" var="flight">
                        <tr>
                            <td><c:out value="${flight.id}"/></td>
                            <td><p id="arrivalTime${flight.id}"></p></td>
                            <script type="text/javascript">
                                getLocalTime('${flight.arrivalDate}', '${flight.arrivalCity.latitude}','${flight.arrivalCity.longitude}', 'arrivalTime${flight.id}');
                            </script>
                        </tr>
                    </c:forEach>
                </tr>
            </table>
        </td>
    </tr>
</table>
</body>
</html>