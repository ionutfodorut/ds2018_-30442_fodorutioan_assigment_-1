<%--
  Created by IntelliJ IDEA.
  User: ioan
  Date: 05.11.2018
  Time: 21:24
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Airline Application</title>
</head>
<body>
    <h1>Flight Management</h1>
    <h2>
        <a href="/new">Add New Flight</a>
        &nbsp;&nbsp;&nbsp;
        <a href="/list">List All Flights</a>

    </h2>
<div align="center">
    <c:if test="${flight != null}">
    <form action="update" method="post">
        </c:if>
        <c:if test="${flight == null}">
        <form action="insert" method="post">
            </c:if>
            <table border="1" cellpadding="5">
                <caption>
                    <h2>
                        <c:if test="${flight != null}">
                            Edit Flight
                        </c:if>
                        <c:if test="${flight == null}">
                            Add New Flight
                        </c:if>
                    </h2>
                </caption>
                <c:if test="${flight != null}">
                    <input type="hidden" name="id" value="<c:out value='${flight.id}' />" />
                </c:if>
                <c:if test="${flight == null}">
                        <tr>
                            <th>Flight ID: </th>
                            <td>
                                <input type="text" name="id" size="10"
                                       value="<c:out value='${flight.id}' />"
                                />
                            </td>
                        </tr>
                </c:if>

                <tr>
                    <th>Airplane Type: </th>
                    <td>
                        <input type="text" name="airplaneType" size="45"
                               value="<c:out value='${flight.airplaneType}' />"
                        />
                    </td>
                </tr>
                <tr>
                    <th>Departure City: </th>
                    <td>
                        <input type="text" name="departureCityName" size="45"
                               value="<c:out value='${flight.departureCity.name}' />"
                        />
                    </td>
                </tr>
                <tr>
                    <th>Departure date: </th>
                    <td>
                        <input type="text" name="departureDate" size="45"
                               value="<c:out value='${flight.departureDate}' />"
                        />
                    </td>
                </tr>
                <tr>
                    <th>Arrival City: </th>
                    <td>
                        <input type="text" name="arrivalCityName" size="45"
                               value="<c:out value='${flight.arrivalCity.name}' />"
                        />
                    </td>
                </tr>
                <tr>
                    <th>Arrival Date: </th>
                    <td>
                        <input type="text" name="arrivalDate" size="45"
                               value="<c:out value='${flight.arrivalDate}' />"
                        />
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="center">
                        <input type="submit" value="Save" />
                    </td>
                </tr>
            </table>
        </form>
</div>
</body>
</html>