<%--
  Created by IntelliJ IDEA.
  User: ioan
  Date: 29.10.2018
  Time: 01:31
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Airline Application</title>
</head>
<body>
    <h1>Flights Management</h1>
    <h2>
        <a href="/new">Add New Flight</a>
        &nbsp;&nbsp;&nbsp;
        <a href="/list">List All Flights</a>

    </h2>
<div align="center">
    <table border="1" cellpadding="5">
        <caption><h2>List of flights</h2></caption>
        <tr bgcolor = "#949494">
            <th>Flight ID</th>
            <th>Airplane type</th>
            <th>Departure city</th>
            <th>Departure date and time</th>
            <th>Arrival city</th>
            <th>Arrival date and time</th>
        </tr>
        <c:forEach items="${flights}" var="flight">
            <tr>
                <td><c:out value="${flight.id}" /></td>
                <td><c:out value="${flight.airplaneType}" /></td>
                <td><c:out value="${flight.departureCity.name}" /></td>
                <td><c:out value="${flight.departureDate}" /></td>
                <td><c:out value="${flight.arrivalCity.name}" /></td>
                <td><c:out value="${flight.arrivalDate}" /></td>
                <td>
                    <a href="${pageContext.request.contextPath}/edit?id=<c:out value='${flight.id}' />">Edit</a>
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <a href="/delete?id=<c:out value='${flight.id}' />">Delete</a>
                </td>
            </tr>
        </c:forEach>
    </table>
</div>
</body>
</html>
