package servlets;

import entities.City;
import entities.Flight;
import service.CityService;
import service.CityServiceImpl;
import service.FlightService;
import service.FlightServiceImpl;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class AdminServlet extends HttpServlet {

    private FlightService flightService = new FlightServiceImpl();
    private CityService cityService = new CityServiceImpl();

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request,response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String action = request.getServletPath();

        try {
            switch (action) {
                case "/new":
                    showNewForm(request, response);
                    break;
                case "/insert":
                    insertFlight(request, response);
                    break;
                case "/delete":
                    deleteFlight(request, response);
                    break;
                case "/edit":
                    showEditForm(request, response);
                    break;
                case "/update":
                    updateFlight(request, response);
                    break;
                default:
                    listFlights(request, response);
                    break;
            }
        } catch (SQLException ex) {
            throw new ServletException(ex);
        }
    }

    private void listFlights(HttpServletRequest request, HttpServletResponse response)
            throws SQLException, IOException, ServletException {
        List<Flight> flightList = flightService.getFlights();
        request.setAttribute("flights", flightList);
        RequestDispatcher dispatcher = request.getRequestDispatcher("admin.jsp");
        dispatcher.forward(request, response);
    }

    private void showNewForm(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        RequestDispatcher dispatcher = request.getRequestDispatcher("form.jsp");
        dispatcher.forward(request, response);
    }

    private void showEditForm(HttpServletRequest request, HttpServletResponse response)
            throws SQLException, ServletException, IOException {
        Long id = Long.parseLong(request.getParameter("id"));
        Flight existingFlight = flightService.getFlight(id);
        RequestDispatcher dispatcher = request.getRequestDispatcher("form.jsp");
        request.setAttribute("flight", existingFlight);
        dispatcher.forward(request, response);

    }

    private void insertFlight(HttpServletRequest request, HttpServletResponse response)
            throws SQLException, IOException {
        Long id = Long.parseLong(request.getParameter("id"));
        String airplaneType = request.getParameter("airplaneType");
        String departureCityName = request.getParameter("departureCityName");
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.ENGLISH);
        Date departureDate = null;
        try {
            departureDate = df.parse(request.getParameter("departureDate"));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String arrivalCityName = request.getParameter("arrivalCityName");
        Date arrivalDate = null;
        try {
            arrivalDate = df.parse(request.getParameter("arrivalDate"));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        City arrivalCity = cityService.getCityByName(arrivalCityName);
        City departureCity = cityService.getCityByName(departureCityName);

        Flight flight = new Flight(id, airplaneType, departureCity, departureDate, arrivalCity, arrivalDate);
        flightService.addFlight(flight);
        response.sendRedirect("/list");
    }

    private void updateFlight(HttpServletRequest request, HttpServletResponse response)
            throws SQLException, IOException {
        Long id = Long.parseLong(request.getParameter("id"));
        String airplaneType = request.getParameter("airplaneType");
        String departureCityName = request.getParameter("departureCityName");
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.ENGLISH);
        Date departureDate = null;
        try {
            departureDate = df.parse(request.getParameter("departureDate"));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String arrivalCityName = request.getParameter("arrivalCityName");
        Date arrivalDate = null;
        try {
            arrivalDate = df.parse(request.getParameter("arrivalDate"));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        City arrivalCity = cityService.getCityByName(arrivalCityName);
        City departureCity = cityService.getCityByName(departureCityName);

        Flight flight = new Flight(id, airplaneType, departureCity, departureDate, arrivalCity, arrivalDate);
        try {
            flightService.updateFlight(flight);
        } catch (Exception e) {
            e.printStackTrace();
        }
        response.sendRedirect("/list");
    }

        private void deleteFlight(HttpServletRequest request, HttpServletResponse response)
            throws SQLException, IOException {
        Long id = Long.parseLong(request.getParameter("id"));

        Flight flight = new Flight();
        flight.setId(id);
        flightService.deleteFlight(flight);
        response.sendRedirect("/list");

    }
}

