package servlets;

import entities.Flight;
import service.FlightService;
import service.FlightServiceImpl;
import service.LoginService;
import service.LoginServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

public class LoginServlet extends HttpServlet {

    private LoginService loginService = new LoginServiceImpl();

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        HttpSession session = request.getSession();
        String page = "/login.jsp";
        if (username.trim().length() >= 0 && username != null && password.trim().length() >= 0 && password != null) {
            String flag = loginService.login(username, password);
            FlightService flightService;
            List<Flight> flightList;
            switch (flag) {
                case "admin":
                    System.out.println("Login success!");
                    request.setAttribute("username", username);
                    request.setAttribute("msg", "Login success!");
                    flightService = new FlightServiceImpl();
                    flightList = flightService.getFlights();
                    request.setAttribute("flights", flightList);
                    session.setAttribute("isUserLoggedIn",true);
                    session.setAttribute("role", "admin");
                    page = "/admin.jsp";
                    break;
                case "client":
                    System.out.println("Login success!");
                    request.setAttribute("username", username);
                    request.setAttribute("msg", "Login success!");
                    flightService = new FlightServiceImpl();
                    flightList = flightService.getFlights();
                    request.setAttribute("flights", flightList);
                    session.setAttribute("isUserLoggedIn",true);
                    session.setAttribute("role", "client");
                    //System.out.println("rOLE: " + session.getAttribute("role"));
                    page = "/client.jsp";
                    break;
                case "false":
                    session.setAttribute("isUserLoggedIn",false);
                    request.setAttribute("msg", "Wrong username or password. Please try again.");
            }
        } else {
            request.setAttribute("msg", "Please complete both fields.");
            session.setAttribute("isUserLoggedIn",false);
        }
        request.getRequestDispatcher(page).include(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //response.sendRedirect("/login.jsp");
    }
}