package dao;

import entities.Flight;

import java.util.List;

public interface FlightsDAO {

    public List<Flight> listAllFlights();

    public int addFlight(Flight flight);

    public Flight deleteFlight(Flight flight);

    public void updateFlight(Flight flight) throws Exception;

    public Flight getFlight(Long id);
}
