package dao;

public interface LoginDAO {

    public String login(String username, String password);
}
