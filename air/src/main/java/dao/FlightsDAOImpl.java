package dao;

import entities.Flight;
import org.hibernate.Session;
import util.HibernateUtil;

import java.util.List;

public class FlightsDAOImpl implements FlightsDAO {

    @Override
    public List<Flight> listAllFlights() {
        Session session = HibernateUtil.getSession();
        System.out.println(session);
        if (session != null ) {
            try {
                List<Flight> flights = session
                        .createQuery("FROM Flight")
                        .list();
                System.out.println(flights);
                session.close();
                return flights;
            } catch (Exception e) {
                System.out.println("Exception occurred while reading user data: " + e);
                session.close();
                return null;
            }
        } else {
            System.out.println("Database server not working!");
        }
        return null;
    }

    public int addFlight(Flight flight) {
        Session session = HibernateUtil.getSession();
        System.out.println(session);
        if (session != null ) {
            try {
                if (flight != null) {
                    session.beginTransaction();
                    int flightId = (Integer) session.save(flight);
                    session.getTransaction().commit();
                    System.out.println(flightId);
                    session.close();
                    return flightId;
                }
            } catch (Exception e) {
                    System.out.println("Exception occurred while writing flight into database: " + e);
                session.close();

                return -1;
            }
        } else {
            System.out.println("Database server not working!");
        }
        return -1;
    }

    public Flight deleteFlight(Flight flight) {
        Session session = HibernateUtil.getSession();
        System.out.println(session);
        if (session != null ) {
            try {
                if (flight != null) {
                    session.beginTransaction();
                    session.delete(flight);
                    session.getTransaction().commit();
                    System.out.println(flight);
                    session.close();
                    return flight;
                }
            } catch (Exception e) {
                System.out.println("Exception occurred while deleting flight from database: " + e);
                session.close();
                return null;
            }
        } else {
            System.out.println("Database server not working!");
        }
        return null;
    }

    public void updateFlight(Flight flight) throws Exception {
        Session session = HibernateUtil.getSession();
        System.out.println(session);
        if (session != null ) {
            if (flight != null) {
                session.beginTransaction();
                session.update(flight);
                session.getTransaction().commit();
                System.out.println("Update succeded!");
                session.close();
            }
        } else {
            session.close();
            System.out.println("Database server not working!");
        }
    }

    public Flight getFlight(Long id) {
        Session session = HibernateUtil.getSession();
        System.out.println(session);
        if (session != null ) {
            try {
                List<Flight> flights = session
                        .createQuery("FROM Flight WHERE id = :id")
                        .setParameter("id", id)
                        .list();
                Flight flight = flights.get(0);
                session.close();
                return flight;
            } catch (Exception e) {
                session.close();
                System.out.println("Exception occurred while reading user data: " + e);
                return null;
            }
        } else {
            System.out.println("Database server not working!");
        }
        return null;
    }
}
