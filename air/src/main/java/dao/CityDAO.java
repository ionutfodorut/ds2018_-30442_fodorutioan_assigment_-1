package dao;

import entities.City;

public interface CityDAO {
    public City getCityByName(String name);
}
