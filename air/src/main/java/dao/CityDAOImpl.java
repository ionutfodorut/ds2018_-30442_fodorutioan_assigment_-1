package dao;

import entities.City;
import org.hibernate.Session;
import util.HibernateUtil;

import java.util.List;

public class CityDAOImpl implements CityDAO {

    @Override
    public City getCityByName(String name) {
        Session session = HibernateUtil.getSession();
        System.out.println(session);
        if (session != null ) {
            try {
                List<City> cities = session
                        .createQuery("FROM City WHERE name = :name")
                        .setParameter("name", name)
                        .list();
                City city = cities.get(0);
                session.close();
                return city;
            } catch (Exception e) {
                session.close();
                System.out.println("Exception occurred while reading user data: " + e);
                return null;
            }
        } else {
            System.out.println("Database server not working!");
        }
        return null;
    }
}
