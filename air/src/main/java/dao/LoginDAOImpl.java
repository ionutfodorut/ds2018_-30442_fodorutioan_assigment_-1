package dao;

import entities.User;
import org.hibernate.Session;
import util.HibernateUtil;

import java.util.List;

public class LoginDAOImpl implements LoginDAO {

    @Override
    public String login(String username, String password) {
        Session session = HibernateUtil.getSession();
        System.out.println(session);
        if (session != null ) {
            try {
                List<User> users = session
                        .createQuery("FROM User WHERE username = :username")
                        .setParameter("username", username)
                        .list();
                User user = users.get(0);
                if (password.equals((user.getPassword()))) {
                    System.out.println("User: " + user.toString());
                    return user.getRole();
                }
            } catch (Exception e) {
                System.out.println("Exception occurred while reading user data: " + e);
                return "false";
            }
        } else {
            System.out.println("Database server not working!");
        }
        return "false";
    }
}
