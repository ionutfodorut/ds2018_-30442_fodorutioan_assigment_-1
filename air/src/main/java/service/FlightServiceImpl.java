package service;

import dao.FlightsDAO;
import dao.FlightsDAOImpl;
import entities.Flight;

import java.util.List;

public class FlightServiceImpl implements FlightService {

    FlightsDAO flightsDAO = new FlightsDAOImpl();

    @Override
    public List<Flight> getFlights() {
        return flightsDAO.listAllFlights();
    }

    @Override
    public int addFlight(Flight flight) {
        return flightsDAO.addFlight(flight);
    }

    @Override
    public Flight deleteFlight(Flight flight) {
        return flightsDAO.deleteFlight(flight);
    }

    @Override
    public void updateFlight(Flight flight) throws Exception {
        flightsDAO.updateFlight(flight);
    }

    @Override
    public Flight getFlight(Long id) {
        return flightsDAO.getFlight(id);
    }
}
