package service;

import dao.LoginDAO;
import dao.LoginDAOImpl;

public class LoginServiceImpl implements LoginService {

    LoginDAO loginDao = new LoginDAOImpl();

    @Override
    public String login(String username, String password) {
        return loginDao.login(username, password);
    }
}
