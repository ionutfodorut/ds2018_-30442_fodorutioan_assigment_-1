package service;

import entities.Flight;

import java.util.List;

public interface FlightService {
    public List<Flight> getFlights();

    public int addFlight(Flight flight);

    public Flight deleteFlight(Flight flight);

    public void updateFlight(Flight flight) throws Exception;

    public Flight getFlight(Long id);
}
