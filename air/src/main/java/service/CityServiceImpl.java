package service;

import dao.CityDAO;
import dao.CityDAOImpl;
import entities.City;

public class CityServiceImpl implements CityService {

    private CityDAO cityDAO = new CityDAOImpl();

    @Override
    public City getCityByName(String name) {
        return cityDAO.getCityByName(name);
    }
}
