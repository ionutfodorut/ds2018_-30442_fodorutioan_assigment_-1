package service;

import entities.City;

public interface CityService {
    public City getCityByName(String name);
}
