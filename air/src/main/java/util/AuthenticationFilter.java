package util;

import javax.servlet.*;
import javax.servlet.Filter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class AuthenticationFilter implements Filter {
    private FilterConfig filterConfig = null;
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        this.filterConfig = filterConfig;
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        HttpSession session = request.getSession(false);
        String loginURI1 = request.getContextPath() + "/login.jsp";
        String loginURI2 = request.getContextPath() + "/login";
        System.out.println("Context path: " + request.getContextPath());
        boolean loggedIn = false;
        String role = null;
        boolean correctRole = true;
        String requestURI = request.getRequestURI();
        if ((session != null) && (session.getAttribute("isUserLoggedIn") != null)) {
            loggedIn = true;
            role = (String) session.getAttribute("role");
            System.out.println("ROle: " + role);
            if (role.equals("admin")) {
                if (requestURI.equals("/client")) {
                    correctRole = false;
                    System.out.println(correctRole);
                }
            } else {
                if (!requestURI.equals("/client")) {
                    correctRole = false;
                    System.out.println(correctRole);
                }
            }
        }
        boolean loginRequest = requestURI.equals(loginURI1) || request.getRequestURI().equals(loginURI2);

        if ((loggedIn && correctRole) || loginRequest) {
            filterChain.doFilter(request, response);
        } else {
            response.sendRedirect("/login.jsp");
        }
    }

    @Override
    public void destroy() {
        this.filterConfig = null;
    }
}
